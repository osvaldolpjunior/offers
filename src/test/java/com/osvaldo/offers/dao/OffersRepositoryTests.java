package com.osvaldo.offers.dao;

import com.osvaldo.offers.model.Offers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class OffersRepositoryTests {

    @Autowired
    private OffersRepository offersRepository;

    private Offers testOffers;

    @Before
    public void setUp(){
        UUID id = UUID.randomUUID();
        testOffers = new Offers(id,
                "Test description",
                189,
                "GBP",
                new Date(),
                "active");
        offersRepository.deleteAll();
    }

    @Test
    public void saveOffers(){
        offersRepository.save(testOffers);
        assertThat(testOffers.getId()).isNotNull();
    }

    @Test
    public void deleteOffers(){
        offersRepository.saveAndFlush(testOffers);
        offersRepository.deleteAll();
        assertThat(offersRepository.findAll()).isEmpty();
    }

    @Test
    public void findAllShouldReturnOffers() throws Exception {
        offersRepository.saveAndFlush(testOffers);
        List<Offers> offersList = offersRepository.findAll();
        Offers actualOffers = offersList.get(0);

        assertThat(offersList.size()).isEqualTo(1);
        assertThat(testOffers.getDescription()).isEqualTo(actualOffers.getDescription());
    }
}
