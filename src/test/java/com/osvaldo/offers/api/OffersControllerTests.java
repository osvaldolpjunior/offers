package com.osvaldo.offers.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.osvaldo.offers.model.Offers;
import com.osvaldo.offers.service.OffersService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class OffersControllerTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private OffersService offersService;

    @Test
    public void offersById_WhenOffersIsFound_ThenReturnOffers() throws Exception {
        UUID id = UUID.randomUUID();
        Offers testOffers = new Offers(id,
                "Test description",
                189,
                "GBP",
                new Date(),
                "active");

        when(offersService.getOffersById(id))
                .thenReturn(Optional.of(testOffers));

        mvc.perform(get("/api/v1/offers/{id}", id))
                .andExpect((status().isOk()))
                .andExpect(jsonPath("$.description", is("Test description")))
                .andExpect(jsonPath("$.status", is("active")));
    }

    @Test
    public void offersById_WhenOffersNotFound_ThenReturnHttp404() throws Exception {
        UUID id = UUID.randomUUID();
        when(offersService.getOffersById(id))
                .thenReturn(Optional.empty());

        mvc.perform(get("/api/v1/offers/{id}", id))
                .andExpect(status().isNotFound());

        verify(offersService, times(1)).getOffersById(id);
        verifyNoMoreInteractions(offersService);
    }

    @Test
    public void whenPostRequestWithValidOffers_ThenReturnOK() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        UUID id = UUID.randomUUID();
        Offers testOffers = new Offers(id,
                "Test description",
                189,
                "GBP",
                new Date(),
                "active");
        String offersRequestJson = mapper.writeValueAsString(testOffers);

        given(offersService.addOffers(any())).willReturn(testOffers);

        mvc.perform(post("/api/v1/offers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(offersRequestJson))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.description", is("Test description")))
                .andExpect(jsonPath("$.status", is("active")));
    }
}
