package com.osvaldo.offers.api;

import com.osvaldo.offers.model.Offers;
import com.osvaldo.offers.service.OffersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(path="api/v1/offers", produces="application/json")
public class OffersController {

    @Autowired
    OffersService offersService;

    @GetMapping
    private List<Offers> getAllOffers(){
        return offersService.getAllOffers();
    }

    @GetMapping(path = "{id}")
    private ResponseEntity<Offers> getOffersById(@PathVariable("id") final UUID id){
        Optional<Offers> optionalOffers = offersService.getOffersById(id);
        if(optionalOffers.isPresent()){
            return new ResponseEntity<>(optionalOffers.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
    }

    @PostMapping(consumes="application/json")
    @ResponseStatus(HttpStatus.CREATED)
    private Offers addOffers(@RequestBody final Offers offers){
        return offersService.addOffers(offers);
    }

    @PutMapping(path="{id}", consumes="application/json")
    private Offers putOffers(@PathVariable("id") final UUID id,@RequestBody final Offers offersToUpdate){
        return offersService.updateOffers(id,offersToUpdate);
    }

    @PatchMapping(path="{id}", consumes="application/json")
    private Offers updateOffers(@PathVariable("id") final UUID id,@RequestBody final Offers offersToUpdate){
        return offersService.updateOffers(id,offersToUpdate);
    }

    @DeleteMapping(path = "{id}")
    @ResponseStatus(code=HttpStatus.NO_CONTENT)
    private void deleteOffers(@PathVariable("id")final UUID id){
        offersService.deleteOffers(id);
    }

}
