package com.osvaldo.offers.model;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table
public class Offers {
    @Id
    @GeneratedValue
    private UUID id;

    @Column(nullable = false, length = 255, unique = true)
    private String description;

    private long price;

    private String currency;

    private Date expirationDate;

    private String status;

    public Offers() {
    }

    public Offers(final UUID id, final String description, final long price, final String currency, final Date expirationDate, final String status) {
        this.id = id;
        this.description = description;
        this.price = price;
        this.currency = currency;
        this.expirationDate = expirationDate;
        this.status = status;
    }

    public UUID getId() {
        return id;
    }

    public void setId(final UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(final long price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(final String currency) {
        this.currency = currency;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(final Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(final String status) {
        this.status = status;
    }
}
