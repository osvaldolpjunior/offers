package com.osvaldo.offers.error;

import java.util.UUID;

public class OffersNotFoundException extends RuntimeException {
    public OffersNotFoundException(final UUID id) {
        super("Offers not found for id: "+id);
    }
}
