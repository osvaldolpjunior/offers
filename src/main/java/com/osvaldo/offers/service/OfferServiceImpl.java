package com.osvaldo.offers.service;

import com.osvaldo.offers.dao.OffersRepository;
import com.osvaldo.offers.error.OffersNotFoundException;
import com.osvaldo.offers.model.Offers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class OfferServiceImpl implements OffersService{

    public static final String ACTIVE = "active";
    public static final String INACTIVE = "inactive";
    @Autowired
    OffersRepository offersRepository;

    @Override
    public List<Offers> getAllOffers() {
        List<Offers> allOffers = new ArrayList<>();
        offersRepository.findAll().forEach(offer ->
                {
                    setExpiredOffers(offer);
                    allOffers.add(offer);
                });
        return allOffers;
    }

    @Override
    public Optional<Offers> getOffersById(final UUID id) {
        Offers validatedOffer = offersRepository.findById(id).orElseThrow(() -> new OffersNotFoundException(id));
        return Optional.ofNullable(setExpiredOffers(validatedOffer));
    }

    @Override
    public Offers addOffers(final Offers offers) {
        Offers addedOffers = offersRepository.save(offers);
        return addedOffers;
    }

    @Override
    public Offers updateOffers(final UUID id, final Offers offersToUpdate) {
        Offers updatedOffers = offersRepository.findById(id).orElseThrow(() -> new OffersNotFoundException(id));
        offersRepository.saveAndFlush(offersToUpdate);
        return updatedOffers;
    }

    @Override
    public void deleteOffers(final UUID id) {
        offersRepository.deleteById(id);
    }

    @Override
    public Offers setExpiredOffers(final Offers offers) {
        if(new Date().after(offers.getExpirationDate()) && offers.getStatus().equals(ACTIVE)){
            final Offers updatedOffers = offers;
            updatedOffers.setStatus(INACTIVE);
            updateOffers(updatedOffers.getId(),updatedOffers);
            return updatedOffers;
        }
        return offers;
    }
}
