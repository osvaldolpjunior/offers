package com.osvaldo.offers.service;

import com.osvaldo.offers.model.Offers;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OffersService {

    List<Offers> getAllOffers();

    Optional<Offers> getOffersById(final UUID id);

    Offers addOffers(final Offers offers);

    Offers updateOffers(final UUID id, final Offers offersToUpdate);

    void deleteOffers(final UUID id);

    Offers setExpiredOffers(final Offers offers);


}
