package com.osvaldo.offers.dao;

import com.osvaldo.offers.model.Offers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
@Repository
public interface OffersRepository extends JpaRepository<Offers, UUID> {
    List<Offers> findAll();

    Optional<Offers> findById(UUID id);

}
