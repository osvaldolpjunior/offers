# Simple Offers API

Simple Offers API is a Spring API for CRUD operations.

## Requirements

For building and running the application you need:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven](https://maven.apache.org)
- [H2 Database](https://www.h2database.com/html/main.html)


### How to Run:

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `com.osvaldo.offers` class from your IDE.



Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
mvn spring-boot:run
```

### How to Run Tests:

```shell
mvn test
```

### How to Run via Postman:

To execute the JSON you can use the [Postman](https://www.getpostman.com/), below are listed the JSON to verify the API.

####  POST  localhost:8080/api/v1/offers/
Create a new Offer.
```json
{
    "description":"Friendly Offer Description",
    "price":19,
    "currency":"GBP",
    "expirationDate":"2020-12-31T23:59:59+0000",
    "status":"active"
}
```
---

####  PUT localhost:8080/api/v1/offers/
Change a Status manually.
```json
{	
    "description":"Friendly Offer Description",
    "price":19,
    "currency":"GBP",
    "expirationDate":"2020-12-31T23:59:59+0000",
    "status":"inactive"
}
```
---

#### GET localhost:8080/api/v1/offers/
Retrieve all existent offers.

---
#### GET localhost:8080/api/v1/offers/d7c78586-543b-4de7-8859-a82720d6d5e2
Retrieve a specific offers.

---
####  DELETE localhost:8080/offers/d7c78586-543b-4de7-8859-a82720d6d5e2 
Remove a specific offers.

---
#### Assumptions made
During the process of get an offer, if the date is expired, the offer is automatically updated to inactive status.


## Author

* **Osvaldo Luis P. Junior** - [Linkedin](https://www.linkedin.com/in/osvaldolpjunior)